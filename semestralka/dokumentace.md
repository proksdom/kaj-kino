## Cíl projektu
Cílem projektu bylo hlavně se naučit používat React a Redux. Zároveň vypracovat semestrální práci dle vlastního zadání. Zadáním bylo vypracovat webovou stránku pro kino s jedním kinosálem, kde si uživatel budí moci vybrat film a následně sedadlo / sedadla na které se chce posadit.


## Popis funkčnosti
Každá stránka až na výběr sedadla obsahuje navigační panel, kde si může uživatel vybrat, kam se přesměruje.

Homepage obnsahuje pouze menší easter egg, jelikož jsem nevěděl co na tuto stránku dát v hodnocení semestrálky bylo i vykreslování SVG pomocí JS a zároveň nějaké CSS transitions a transformace. Po najetí na čtvereček se čtvereček náhodně překreslí na jiné pozici. Po najetí na nápis "Click the rectangle to choose film" se zapne CSS animace, která textem začne rotovat a změní jeho barvu.

About stránka obsahuje pouze pravdivé informace (omlouvám se, zda to někoho urazí).

Stránka Films obsahuje formulář na filtraci filmů podle data promítání a zároveň vypsané všechny filmy, které se budou promítat nebo byli promítané (filmy se řadí od nejstaršího po nejnovější podle data promítání)
Kliknutí na Get a seat! button přesměruje uživatele na stránku s plátnem a sedadly.

Stránka s plátnem:
zde si může uživatel naklikat jednu nebo více sedadel, kde by chtěl sedět. Po kliknutí na button save v pravém horním rohu se jeho výběr uloží do global storage a zároveň do sessionStorage a uživatel je přesměrován zpět na stránku s výpisem filmů (Films).