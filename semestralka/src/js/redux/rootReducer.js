//nahraje data ze sessionStorage
const getSessionStorage = () => {
    let a = JSON.parse(sessionStorage.getItem('films'));
    return a;
}

//počáteční stav global storage, pokud nic není uloženo v sessionStorage prohlížeče
//data tu jsou na pevno napsaná, protože nepoužívám žádné API na stažení dat, tak jsem je napsal ručně, aby zde vůbec nějaká data byla
const initState = {
    films: getSessionStorage() || [
        { id:1, name: "Pink Panther", date: "2020-06-10T16:00:00Z",occupied:[]},
        { id:2, name: "Fast & Furious", date: "2020-07-10T16:00:00Z",occupied:[]},
        { id:3, name: "Harry Potter", date: "2020-08-10T16:00:00Z",occupied:[]},
        { id:4, name: "How i met your mother", date: "2020-09-10T16:00:00Z",occupied:[]},
        { id:5, name: "Avengers", date: "2020-10-10T16:00:00Z",occupied:[]},
        { id:6, name: "Joker", date: "2020-11-10T16:00:00Z",occupied:[]},
        { id:7, name: "Parasite", date: "2020-12-10T16:00:00Z",occupied:[]},
        { id:8, name: "Extraction", date: "2020-06-11T16:00:00Z",occupied:[]},
        { id:9, name: "The Lodge", date: "2020-06-12T17:00:00Z",occupied:[]},
        { id:10, name: "Pink Panther", date: "2020-06-10T19:00:00Z",occupied:[]},
        { id:11, name: "Fast & Furious", date: "2020-07-10T19:00:00Z",occupied:[]},
        { id:12, name: "Harry Potter", date: "2020-08-10T19:00:00Z",occupied:[]},
        { id:13, name: "How i met your mother", date: "2020-09-10T19:00:00Z",occupied:[]},
        { id:14, name: "Avengers", date: "2019-10-10T19:00:00Z",occupied:[]},
        { id:15, name: "Joker", date: "2018-11-10T19:00:00Z",occupied:[]},
        { id:16, name: "Parasite", date: "2020-11-11T14:00:00Z",occupied:[]},
        { id:17, name: "Extraction", date: "2019-06-11T15:00:00Z",occupied:[]},
        { id:18, name: "The Lodge", date: "2020-05-12T16:00:00Z",occupied:[]},
        { id:19, name: "Extraction", date: "2019-06-12T16:00:00Z",occupied:[]},
    ]
}

const rootReducer = (state = initState, action) => {
    //uloží nově vytvoření stav do global storage reduxu a zároveň jej uloží do sessionStorage
    if(action.type === 'ADD_OCCUPIED'){
        let films = state.films.filter(film => true);
        let film = films.find(f => f.id === action.film_id);
        film.occupied[action.seat_id] = true;
        sessionStorage.setItem('films',JSON.stringify(films))
        return {
            ...state,
            films: films
        }
    }
    return state;
}
    
export default rootReducer;