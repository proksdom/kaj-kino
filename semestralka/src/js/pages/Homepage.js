import React, { Component } from "react";


import Navigation from '../components/Navigation';
import Footer from '../components/Footer';
import HomepageContent from '../components/HomepageContent';
//vykreslení homepage
class Homepage extends Component {
	render() {
		return (
			<div className="page-wrapper">
				<Navigation/>
				<HomepageContent/>
				<Footer/>
			</div>
			
		);
	}
}

export default Homepage;