import React, { Component } from "react";


import Navigation from '../components/Navigation';
import Footer from '../components/Footer';

class About extends Component {
    //vykreslí About page
    render() {
        return (
            <div className="about-wrapper">
                <Navigation />
                    <div className="about-content-container">
                        <p className="about-content">
                            Semestrálka z KAJů, která bude hodnocená panem Husťákem. Jedná se o hodně marnej pokus a dlouhý hodiny brečení při vývoji frontendu, kterej mě nikdy neposlouchá...
                        </p>
                    </div>
                <Footer />
            </div>
        );
    }
}

export default About;