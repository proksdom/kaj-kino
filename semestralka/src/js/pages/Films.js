import React, { Component } from "react";


import Navigation from '../components/Navigation';
import Footer from '../components/Footer';
import FilmsCards from '../components/FilmsCards';

class Films extends Component {
    //vykreslí page s nabídkou filmů
	render() {
		return (
            <div className="films-wrapper">
                <Navigation/>
                <FilmsCards/>
                <Footer/>
            </div>
			
		);
	}
}

export default Films;