import React from 'react'
import { connect } from 'react-redux';
import Seat from '../components/Seat';
import zvuk from '../../Click2.mp3';

class Hall extends React.Component {
    //inicializace objektu
    constructor(props){
        super(props);
        this.state = {
            seats:[],
            play: false
        };
        this.sound = new Audio(zvuk);
    }
    //přidání eventů pro zvuk a překreslování na resize event
    componentDidMount(){
        this.onResize();
        this.sound.addEventListener('ended', () => this.setState({ play: false }));
        window.addEventListener('resize',this.onResize);
    }
    //odebraní eventů pro zvuk a překreslování na resize event
    componentWillUnmount(){
        window.removeEventListener('resize',this.onResize);
        this.sound.addEventListener('ended', () => this.setState({ play: false }));
    }
    //zapínání / vypínání přehrávání zvuku po kliknutí na sedadlo
    togglePlay = () => {
        this.setState({play: !this.state.play}, () =>{
            this.state.play ? this.sound.play() : this.sound.pause();
        });
    }
    //zapne zvuk po kliknutí na sedadlo a zároveň mu nastaví příznak "obsazeno", sedadlo se potom překreslí na zeleno a uloží se do storage jako obsazené sedadlo (pokud to podmínky povolují)
    handleSeatClick = (id) => {
        this.togglePlay();
        //zavolá přidání příznaku obsazenosti v redux storu (storage)
        this.props.addSeatOccupied(this.props.film.id,id);
    }
    //přesměrování na stránku s filmy
    redirect = () => {
        this.props.history.push("/films");
    }
    //překreslí stránku na event onresize
    onResize = () => {
        this.svg = document.querySelector("#hall-svg");
        this.svg.setAttributeNS(null, "width",window.innerWidth);
        this.svg.setAttributeNS(null,"height",window.innerHeight);
        this.createSeats();
    }
    //vypočítá rozpoložení sedačle a vykreslí je
    createSeats = () => {
        //startovací pozice sedadel (polovina výšky obrazovky)
        let startPos = Math.floor(window.innerHeight / 2);
        //šířka a výška sedadla
        let seatWidth = Math.floor(window.innerWidth / 11);
        let seatHeight = Math.floor(window.innerHeight / 9);
        //margin sedadel
        let seatXMargin = 15;
        let seatYMargin = 10;
        //"ukazatele" na sedadla v poli
        let x = 0;
        let y = 0;
        //inicializace pole sedadel
        let seats = [];
        //id je potřeba kvůli reactu, který potřebuje přidělovat klíče k podobným nebo stejným komponentám (řvalo to na mě v konzoli)
        let id = 0;
        //tu se děje magie
        for(let i = 0; i < 4; i++){
            for(let j = 0; j < 10; j++){
                x = Math.floor(((j+1)*seatXMargin)+(j*seatWidth));
                y = startPos+Math.floor((i*seatYMargin)+(Math.floor((i))*seatHeight));
                let occupied = false;
                //nastavení příznaku obsazenosti pro zrovna vykreslované sedadlo podle toho, zda už bylo uloženo ve storage či nikoliv
                if(this.props.film.occupied[id]){
                    occupied = true;
                }
                //přidání sedačky do pole sedadel pro následné vykreslení, zároveň sedadlu nastavuje všechny potřebné atributy
                seats.push(<Seat id={id} x={x} y={y} height = {seatHeight} width = {seatWidth} key = {id} onClick={this.handleSeatClick} occupied={occupied}></Seat>);
                id++;
            }
        }
        //nastaví nový stav komponenty (do stavu se uloží vytvořené sedačky a stránka se překreslí)
        this.setState({
            seats: seats
        });
    }
    //vykreslení stránky, první rectangle představuje promítací plátno
    render() {
        return (
            <div className="hall-content-wrapper">
                <svg id="hall-svg">
                    <rect x="0" y="0" height = "150px" width={window.innerWidth} fill="gray" id="screen-rect"></rect>
                    {this.state.seats}
                </svg>
                <button id="hall-button" className="btn btn-primary" onClick={this.redirect}>Save</button>
            </div>
        );
    }
}
//metoda pro redux, která načítá global state z redux storu
const mapStateToProps = (state,ownProps) => {
    let id = ownProps.match.params.film_id;
    return {
        film: state.films.find( (film) => film.id == id)
    };
}
//metoda pro redux, která zapisuje změny do global state
const mapDispatchToProps = (dispatch) => {
    return {
        addSeatOccupied: (film_id, seat_id) => {
            dispatch({type: 'ADD_OCCUPIED', film_id: film_id, seat_id:seat_id})
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Hall);