import React from 'react';

class HomepageContent extends React.Component {
    //změní pozici čtverečku na eventu mouseover
    handleMouseOver = (e) => {
        const bBox = this.svg.getBoundingClientRect();
        const x = Math.floor(Math.random() * bBox.width);
        const y = Math.floor(Math.random() * bBox.height);

        this.square.setAttributeNS(null,"x",x);
        this.square.setAttributeNS(null,"y",y);
    }
    //inicializace SVG "easter eggu"
    componentDidMount() {
        this.svg = document.querySelector("#svg-easter-egg");
        this.svgNS = "http://www.w3.org/2000/svg";
        this.square = document.createElementNS(this.svgNS, "rect");
        this.square.addEventListener("mouseover", this.handleMouseOver);
        
        this.square.setAttributeNS(null, "fill", "red");
        this.square.setAttributeNS(null, "width", 50);
        this.square.setAttributeNS(null, "height", 50);
        this.svg.appendChild(this.square);
        this.handleMouseOver();
    }


    render() {
        return (
            <div className="homepage-content-wrapper">
                <div className="header-container">
                    <h2 className="header-text">Click the rectangle to choose a film</h2>
                </div>
                <svg id="svg-easter-egg" width="100vw" height="600px" >

                </svg>
            </div>
        );
    }
}
export default HomepageContent;
