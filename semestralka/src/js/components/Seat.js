import React from 'react';

class Seat extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            id: this.props.id,
            occupied: this.props.occupied
        };
    }
    //změna barvy  čtverečku představující sedadlo na eventu mouseclick podle toho, zda už bylo sedadlo načteno jako obsazené či nikoliv
    //pokud bylo sedadlo načtené (vykreslené) jako oobsazené, tak se barva sedadla nastaví na červenou, jinak na zelenou
    handleClick = (e) => {
        if(!this.props.occupied){
            if(!this.state.occupied){
                
                this.setState({
                    ...this.state,
                    occupied: true
                });


                e.target.setAttributeNS(null,"fill","green");
                this.props.onClick(this.state.id);
            } else {

                this.setState({
                    ...this.state,
                    occupied: false
                });


                e.target.setAttributeNS(null,"fill","red");
            }
        }
        
    }


    render() {
        return (
            <rect width={this.props.width} height={this.props.height} className="seat" x={this.props.x} y={this.props.y} onClick={this.handleClick} fill={this.state.occupied?this.props.occupied?"red":"green":"lightgray"}>

            </rect>
        );
    }
}

export default Seat