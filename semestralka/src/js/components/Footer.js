import React, { Component } from "react";
import '../../css/styles.css';

//vykreslení footeru
class Footer extends Component {
    render() {
        return (
            <div className="footer-container">
                <footer className="fixed-bottom ">
                    <div className="container-fluid text-center bg-secondary text-white">
                        &copy;Dominik Prokš
                    </div>
                </footer>
            </div> 
        );
    }
}

export default Footer;