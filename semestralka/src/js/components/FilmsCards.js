import React, { Component } from "react";
import FilmCard from './FilmCard';
import { connect } from 'react-redux';
import DatePicker from 'react-datepicker';
import Button from 'react-bootstrap/Button';

import 'react-datepicker/dist/react-datepicker.css';

class FilmsCards extends Component {
    constructor(props) {
        super(props);
        this.state = {
            date: new Date(),
            filter: false
        }
    }
    //zapnutí filtrování s určitým datem
    dateSelected = (date) => {
        this.setState({
            date: date,
            filter: true
        });
    }
    //vypnutí filtrování
    resetDateSelected = () => {
        this.setState({
            filter: false
        });
    }

    render() {
        return (
            <div className="films-cards-wrapper container">
                <div className="d-flex justify-content-start align-items-center mt-2">
                    <div className="p-2"><label htmlFor="datepicker">Filter by date:</label></div>
                    <div className="p-2">
                        <DatePicker id="datepicker"
                            selected={this.state.date}
                            onChange={date => this.dateSelected(date)}
                            dateFormat="dd MMMM yyyy"
                        /></div>
                    <div className="ml-auto p-2"><Button onClick={this.resetDateSelected}>Reset</Button></div>
                </div>

                <div className="row justify-content-center">
                    {this.props.films && this.props.films.map((film) => {
                        if (this.state.filter) {
                            if (this.state.date.getUTCDate() === new Date(film.date).getUTCDate() && this.state.date.getUTCMonth() === new Date(film.date).getUTCMonth() && this.state.date.getUTCFullYear() === new Date(film.date).getUTCFullYear()) {//podmínka na filtrování
                                return <FilmCard film={film} key={film.id} />;
                            }
                        } else {
                            return <FilmCard film={film} key={film.id} />;
                        }
                        return null;
                    })}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        films: state.films.sort(function(a,b){return new Date(a.date) - new Date(b.date)})
    }
}

export default connect(mapStateToProps)(FilmsCards);