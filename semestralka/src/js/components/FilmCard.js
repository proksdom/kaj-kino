import React, { Component } from "react";
import NavLink from 'react-router-dom/NavLink';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';

//komponenta pro vykreslení karty filmu
//používám zde react-bootstrap komponenty, jelikož mi nefungoval samotný bootstrap
class FilmCard extends Component {
    render() {
        return (
            <div className="col-md-3 mt-4">
                <Card className="text-center">
                    <Card.Body>
                        <Card.Title>{this.props.film.name}</Card.Title>
                        <Card.Text>{new Date(this.props.film.date).toUTCString()}</Card.Text>
                        <Button variant="secondary">
                            <NavLink to={"/hall/"+this.props.film.id} className="nav-link text-warning">Get a seat!</NavLink>
                        </Button>
                    </Card.Body>
                </Card>
            </div>
        );
    }
}

export default FilmCard;