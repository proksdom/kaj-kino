import React from 'react';
import {
  BrowserRouter as Router,
  Route,
} from "react-router-dom";
import Homepage from './js/pages/Homepage';
import About from './js/pages/About';
import Films from './js/pages/Films';
import Hall from './js/pages/Hall'

//React router se zde stará o vykreslování správných komponent podle URL na které se aktuálně uživatel nachází
function App() {
  return (
      <Router>
        <Route exact path="/" component={Homepage} />
        <Route exact path="/about" component={About} />
        <Route exact path="/films" component={Films} />
        <Route path="/hall/:film_id" component={Hall}/>
      </Router>
  );
}

export default App;
